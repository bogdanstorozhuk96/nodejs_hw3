const jwt = require("jsonwebtoken");
const config = require("config");
const nodemailer = require("nodemailer");

const User = require("../models/user");
const { hashPassword, checkHashPassword } = require("../utils");

const { secret } = config.get("Auth");

module.exports.register = async (request, response) => {
  const { email, password, role } = request.body;
  try {
    const user = new User({
      email,
      password: await hashPassword(password),
      created_date: new Date().toISOString(),
      role,
    });
    await user.save();
    response.json({ message: "Profile created successfully" });
  } catch (error) {
    response.status(500).json({ message: error.message });
  }
};

module.exports.login = async (request, response) => {
  const { email, password } = request.body;
  try {
    const user = await User.findOne({ email });
    if (!user) {
      throw {
        status: 400,
        message: "No user with such email",
      };
    }
    const isSame = await checkHashPassword(password, user.password);
    if (!isSame) {
      throw {
        status: 400,
        message: "Incorrect password",
      };
    }
    response.json({
      jwt_token: jwt.sign(JSON.stringify(user), secret),
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.forgotPassword = async (request, response) => {
  const { email } = request.body;
  try {
    const user = await User.findOne({ email });
    if (!user) {
      throw {
        status: 400,
        message: "No user with such email",
      };
    }
    const newPassword = Math.random().toString(36).slice(-8);
    const name = email.substring(0, email.indexOf("@"));
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "nodejsEmailSender123@gmail.com",
        pass: "supharpass123",
      },
    });
    const mailOptions = {
      from: "nodejsEmailSender123@gmail.com",
      to: email,
      subject: "Your new password just arrived",
      html: `<h1>Hi, ${name}</h1><p>You can change the password on website. Here is the new password: ${newPassword}</p>`,
    };
    await transporter.sendMail(mailOptions);
    await User.updateOne(user, { password: await hashPassword(newPassword) });
    response.json({
      message: "New password sent to your email address",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};
