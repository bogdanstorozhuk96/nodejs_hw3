const User = require("../models/user");
const { findUserById } = require("../utils");
const { hashPassword, checkHashPassword } = require("../utils");

module.exports.getUser = async (request, response) => {
  const { _id: currentUserId } = request.user;
  try {
    const user = await findUserById(currentUserId);
    const { _id, email, created_date, role } = user;
    response.json({
      user: {
        _id,
        email,
        created_date,
        role,
      },
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.deleteUser = async (request, response) => {
  const { _id: currentUserId } = request.user;
  try {
    const user = await findUserById(currentUserId);
    await User.deleteOne(user);
    response.json({
      message: "Profile deleted successfully",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.changeUserPassword = async (request, response) => {
  const { _id: currentUserId } = request.user;
  const { oldPassword, newPassword } = request.body;
  try {
    const user = await findUserById(currentUserId);
    const isSame = await checkHashPassword(oldPassword, user.password);
    if (!isSame) {
      throw {
        status: 403,
        message: "Old password is not equal to the current one",
      };
    }
    await User.updateOne(user, { password: await hashPassword(newPassword) });
    response.json({
      message: "Password changed successfully",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};
