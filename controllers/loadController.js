const Load = require("../models/load");
const Truck = require("../models/truck");
const {
  findUserById,
  findLoadById,
  checkIfLoadFitsInTrucks,
} = require("../utils");
const {
  sprinter,
  smallStraight,
  largeStraight,
  loadStates,
} = require("../utils/dataTypes");

module.exports.addLoad = async (request, response) => {
  const {
    dimensions,
    payload,
    name,
    pickup_address,
    delivery_address,
  } = request.body;
  const { _id } = request.user;
  try {
    const load = new Load({
      created_by: _id,
      status: "NEW",
      dimensions,
      payload,
      name,
      pickup_address,
      delivery_address,
      created_date: new Date().toISOString(),
    });
    await load.save();
    response.json({ message: "Load created successfully" });
  } catch (error) {
    response.status(500).json({ message: error.message });
  }
};

module.exports.getLoadById = async (request, response, next) => {
  const { id: loadId } = request.params;
  const { _id: currentUserId } = request.user;
  if (loadId) {
    try {
      const load = await findLoadById(loadId, currentUserId);
      response.json({
        load,
      });
    } catch (error) {
      if (error.status !== undefined) {
        response.status(error.status).json({ message: error.message });
      } else {
        response.status(500).json({ message: error.message });
      }
    }
  } else {
    next();
  }
};

module.exports.getCreatedLoads = async (request, response) => {
  const { _id: currentUserId } = request.user;
  let { status, limit, offset } = request.query;
  try {
    const user = await findUserById(currentUserId);
    let loads;
    if (user.role === "SHIPPER") {
      loads = await Load.find({ created_by: currentUserId });
    } else if (user.role === "DRIVER") {
      loads = await Load.find({ assigned_to: currentUserId });
    }
    if (status) {
      loads = loads.filter((load) => load.status === status);
    }
    if (offset) {
      loads.splice(0, offset);
    }
    if (!limit) {
      limit = 10;
    }
    if (limit) {
      loads = loads.slice(0, limit);
    }
    response.json([{ loads }]);
  } catch (error) {
    response.status(500).json({ message: error.message });
  }
};

module.exports.updateLoad = async (request, response) => {
  const { id: loadId } = request.params;
  const { _id: currentUserId } = request.user;
  try {
    const load = await findLoadById(loadId, currentUserId);
    if (load.assigned_to || load.status !== "NEW") {
      throw {
        status: 400,
        message:
          "You can not update this load because its already assigned to a driver",
      };
    }
    await Load.updateOne(load, request.body);
    response.json({
      message: "Load details changed successfully",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.deleteLoadWithStatusNewById = async (request, response) => {
  const { id: loadId } = request.params;
  const { _id: currentUserId } = request.user;
  try {
    const load = await findLoadById(loadId, currentUserId);
    if (load.assigned_to || load.status !== "NEW") {
      throw {
        status: 400,
        message:
          "You can not delete this load because its already assigned to a driver",
      };
    }
    await Load.deleteOne(load);
    response.json({
      message: "Load deleted successfully",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.postLoad = async (request, response) => {
  const { id: loadId } = request.params;
  const { _id: currentUserId } = request.user;
  try {
    const load = await findLoadById(loadId, currentUserId);

    if (load.status !== "NEW") {
      throw {
        status: 400,
        message: "This load is not suitable for posting",
      };
    }

    await Load.updateOne(load, { status: "POSTED" });

    const availableTrucks = await Truck.find({
      status: "IS",
    });
    if (availableTrucks.length === 0) {
      const load = await findLoadById(loadId, currentUserId);
      await Load.updateOne(load, { $set: { status: "NEW" } });
      load.logs.push({
        message: "Unsuccessful posting attemp. No trucks in service",
        time: new Date().toISOString(),
      });
      await load.save();
      throw {
        status: 400,
        message: "There are no available trucks atm",
      };
    }
    const filteredTrucks = availableTrucks.filter((truck) => {
      if (!truck.assigned_to) {
        return false;
      }
      switch (truck.type) {
        case "SPRINTER":
          return checkIfLoadFitsInTrucks(
            sprinter,
            load.dimensions,
            load.payload
          );
        case "SMALL STRAIGHT":
          return checkIfLoadFitsInTrucks(
            smallStraight,
            load.dimensions,
            load.payload
          );
        case "LARGE STRAIGHT":
          return checkIfLoadFitsInTrucks(
            largeStraight,
            load.dimensions,
            load.payload
          );
      }
    });
    if (filteredTrucks.length === 0) {
      const load = await findLoadById(loadId, currentUserId);
      await Load.updateOne(load, { $set: { status: "NEW" } });
      load.logs.push({
        message:
          "Unsuccessful posting attemp. There are no trucks that can fit this load or not of the assigned",
        time: new Date().toISOString(),
      });
      await load.save();
      throw {
        status: 400,
        message:
          "There are no trucks that can fit this load or not of the assigned",
      };
    }
    const truck = filteredTrucks[0];
    await Truck.updateOne(truck, { status: "OL" });
    load.status = "ASSIGNED";
    load.state = "En route to Pick Up";
    load.assigned_to = truck.assigned_to;
    load.logs.push({
      message: "Load was assigned to be picked up by driver",
      time: new Date().toISOString(),
    });
    await load.save();
    response.json({ message: "Load posted successfully", driver_found: true });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.getActiveDriverLoad = async (request, response) => {
  const { _id: currentUserId } = request.user;
  try {
    const activeDriverLoad = await Load.findOne({
      assigned_to: currentUserId,
      status: { $ne: "SHIPPED" },
    });
    response.json({
      activeDriverLoad,
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.iterateToNextLoadState = async (request, response) => {
  const { _id: currentUserId } = request.user;
  let finalOperation = false;
  try {
    const activeDriverLoad = await Load.findOne({
      assigned_to: currentUserId,
      status: { $ne: "SHIPPED" },
    });
    if (!activeDriverLoad) {
      throw {
        status: 400,
        message: "You dont have an active assigned load",
      };
    }
    const index = loadStates.findIndex(
      (item) => item === activeDriverLoad.state
    );
    const newState = loadStates[index + 1];
    await Load.updateOne(activeDriverLoad, {
      $set: { state: newState },
    });
    activeDriverLoad.logs.push({
      message: `Load state changed to '${newState}'`,
      time: new Date().toISOString(),
    });
    await activeDriverLoad.save();
    if (newState === "Arrived to delivery") {
      finalOperation = true;
    }
    if (finalOperation) {
      const truck = await Truck.findOne({
        assigned_to: currentUserId,
      });
      await Truck.updateOne(truck, {
        status: "IS",
      });
      const activeDriverLoad = await Load.findOne({
        assigned_to: currentUserId,
        status: { $ne: "SHIPPED" },
      });
      await Load.updateOne(activeDriverLoad, {
        status: "SHIPPED",
      });
    }
    response.json({
      message: `Load state changed to '${newState}'`,
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.getShippingInfo = async (request, response) => {
  const { _id: currentUserId } = request.user;
  const { id: loadId } = request.params;
  try {
    const load = await findLoadById(loadId, currentUserId);
    if (load.status !== "ASSIGNED") {
      throw {
        status: 400,
        message: "Load is not being shipped atm",
      };
    }
    const truck = await Truck.findOne({
      assigned_to: load.assigned_to,
    });
    if (!truck) {
      throw {
        status: 400,
        message: "Truck does not exist",
      };
    }
    response.json({ load, truck });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};
