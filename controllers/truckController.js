const Truck = require("../models/truck");
const { findTruckById } = require("../utils");

module.exports.addTruck = async (request, response) => {
  const { type } = request.body;
  const { _id } = request.user;
  try {
    const truck = new Truck({
      created_by: _id,
      status: "IS",
      type,
      created_date: new Date().toISOString(),
    });
    await truck.save();
    response.json({ message: "Truck created successfully" });
  } catch (error) {
    response.status(500).json({ message: error.message });
  }
};

module.exports.getTruckById = async (request, response, next) => {
  const { id: truckId } = request.params;
  const { _id: currentUserId } = request.user;
  if (truckId) {
    try {
      const truck = await findTruckById(truckId, currentUserId);
      response.json({
        truck,
      });
    } catch (error) {
      if (error.status !== undefined) {
        response.status(error.status).json({ message: error.message });
      } else {
        response.status(500).json({ message: error.message });
      }
    }
  } else {
    next();
  }
};

module.exports.getCreatedTrucks = async (request, response) => {
  const { _id: currentUserId } = request.user;
  try {
    const trucks = await Truck.find({ created_by: currentUserId });
    response.json([{ trucks }]);
  } catch (error) {
    response.status(500).json({ message: error.message });
  }
};

module.exports.assignTruckToUser = async (request, response) => {
  const { id: truckId } = request.params;
  const { _id: currentUserId } = request.user;
  try {
    const assignedToUserTruck = await Truck.findOne({
      assigned_to: currentUserId,
    });
    await Truck.updateOne(assignedToUserTruck, { assigned_to: null });
    const truck = await findTruckById(truckId, currentUserId);
    if (truck.assigned_to) {
      throw {
        status: 400,
        message: "This truck is already assigned to someone",
      };
    }
    await Truck.updateOne(truck, { assigned_to: currentUserId });
    response.json({
      message: "Truck assigned successfully",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.updateTruckById = async (request, response) => {
  const { id: truckId } = request.params;
  const { type } = request.body;
  const { _id: currentUserId } = request.user;
  try {
    const truck = await findTruckById(truckId, currentUserId);
    await Truck.updateOne(truck, { type });
    response.json({
      message: "Truck details changed successfully",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.deleteTruckById = async (request, response) => {
  const { id: truckId } = request.params;
  const { _id: currentUserId } = request.user;
  try {
    const truck = await findTruckById(truckId, currentUserId);
    await Truck.deleteOne(truck);
    response.json({
      message: "Truck deleted successfully",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};
