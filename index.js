const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const config = require("config");

const { port } = config.get("Server");
const { dbUserUsername, dbUserPassword } = config.get("DbConfig");

const authRouter = require("./routers/authRouter");
const userRouter = require("./routers/userRouter");
const truckRouter = require("./routers/truckRouter");
const loadRouter = require("./routers/loadRouter");

const uri = `mongodb+srv://dbUser:${dbUserPassword}@cluster0.q2grl.mongodb.net/${dbUserUsername}?retryWrites=true&w=majority`;

const app = express();

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

app.use(cors());
app.use(express.json());

app.use("/api", authRouter);
app.use("/api", userRouter);
app.use("/api", truckRouter);
app.use("/api", loadRouter);

app.listen(process.env.PORT || port, () => {
  console.log(`Server started on ${port} port`);
});
