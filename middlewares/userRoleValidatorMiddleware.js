const { findUserById } = require("../utils");

module.exports.shipperValidatorMiddleware = async (request, response, next) => {
  const { _id } = request.user;
  try {
    const user = await findUserById(_id);
    if (user.role === "DRIVER") {
      return response
        .status(400)
        .json({ message: "Driver account cannot perform this action" });
    }
    next();
  } catch (error) {
    return response.status(500).json({ message: error.message });
  }
};

module.exports.driverValidatorMiddleware = async (request, response, next) => {
  const { _id } = request.user;
  try {
    const user = await findUserById(_id);
    if (user.role === "SHIPPER") {
      return response
        .status(400)
        .json({ message: "Shipper account cannot perform this action" });
    }
    next();
  } catch (error) {
    return response.status(500).json({ message: error.message });
  }
};
