const schemas = require("../utils/schemas");

module.exports = async (request, response, next) => {
  const route = request.route.path;
  const schema = schemas[`${route} ${request.method}`];
  if (schema) {
    try {
      await schema.validateAsync(request.body);
      next();
    } catch (error) {
      return response.status(400).json({ message: error.message });
    }
  }
};
