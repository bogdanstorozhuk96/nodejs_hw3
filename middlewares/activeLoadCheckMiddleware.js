const Load = require("../models/load");
const User = require("../models/user");

module.exports = async (request, response, next) => {
  const { _id } = request.user;
  try {
    const user = await User.findById(_id);
    if (user.role === "DRIVER") {
      const activeDriverLoad = await Load.findOne({
        assigned_to: _id,
        status: { $ne: "SHIPPED" },
      });
      if (activeDriverLoad) {
        throw {
          status: 400,
          message: "Driver with active load can not perform this action",
        };
      }
    }
    next();
  } catch (error) {
    return response.status(500).json({ message: error.message });
  }
};
