const jwt = require("jsonwebtoken");
const config = require("config");

const { secret } = config.get("Auth");

module.exports = (request, response, next) => {
  const authHeader = request.headers["authorization"];

  if (!authHeader) {
    return response
      .status(401)
      .json({ message: "No authorization header found" });
  }

  const [, jwtToken] = authHeader.split(" ");

  try {
    request.user = jwt.verify(jwtToken, secret);
    next();
  } catch (error) {
    return response.status(401).json({ message: "Invalid jwt" });
  }
};
