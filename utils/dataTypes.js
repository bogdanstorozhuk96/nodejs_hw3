module.exports.sprinter = {
  width: 300,
  length: 250,
  height: 170,
  payload: 1700,
};
module.exports.smallStraight = {
  width: 500,
  length: 250,
  height: 170,
  payload: 2500,
};
module.exports.largeStraight = {
  width: 700,
  length: 350,
  height: 200,
  payload: 4000,
};

module.exports.loadStates = [
  "En route to Pick Up",
  "Arrived to Pick Up",
  "En route to delivery",
  "Arrived to delivery",
];
