const joi = require("joi");

const authDataSchema = joi.object({
  email: joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net", "ru"] },
  }),
  password: joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")).required(),
  role: joi.string().valid("DRIVER", "SHIPPER").required(),
});

const loginDataSchema = joi.object({
  email: joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net", "ru"] },
  }),
  password: joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")).required(),
});

const forgotPasswordDataSchema = joi.object({
  email: joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net", "ru"] },
  }),
});

const changeUserPasswordSchema = joi.object({
  oldPassword: joi
    .string()
    .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    .required(),
  newPassword: joi
    .string()
    .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    .required(),
});

const addTruckDataSchema = joi.object({
  type: joi
    .string()
    .valid("SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT")
    .required(),
});

const updateTruckDataSchema = joi.object({
  type: joi
    .string()
    .valid("SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT")
    .required(),
});

const addLoadDataSchema = joi.object({
  dimensions: joi.object({
    width: joi.number().required(),
    length: joi.number().required(),
    height: joi.number().required(),
  }),
  payload: joi.number().required(),
  name: joi.string().required(),
  pickup_address: joi.string().required(),
  delivery_address: joi.string().required(),
});

const updateLoadDataSchema = joi.object({
  dimensions: joi
    .object({
      width: joi.number().required(),
      length: joi.number().required(),
      height: joi.number().required(),
    })
    .optional(),
  state: joi
    .string()
    .valid(
      "En route to Pick Up",
      "Arrived to Pick Up",
      "En route to delivery",
      "Arrived to delivery"
    )
    .optional(),
  payload: joi.number().optional(),
  name: joi.string().optional(),
  pickup_address: joi.string().optional(),
  delivery_address: joi.string().optional(),
});

const getCreatedLoadsQuerySchema = joi.object({
  status: joi.string().valid("NEW", "POSTED", "ASSIGNED", "SHIPPED").optional(),
  limit: joi.number().integer().min(10).max(50).optional(),
  offset: joi.number().integer().optional(),
});

module.exports = {
  "/auth/register POST": authDataSchema,
  "/auth/login POST": loginDataSchema,
  "/auth/forgot_password POST": forgotPasswordDataSchema,
  "/users/me/password PATCH": changeUserPasswordSchema,
  "/trucks POST": addTruckDataSchema,
  "/trucks/:id PUT": updateTruckDataSchema,
  "/loads POST": addLoadDataSchema,
  "/loads/:id PUT": updateLoadDataSchema,
  "/loads GET": getCreatedLoadsQuerySchema,
};
