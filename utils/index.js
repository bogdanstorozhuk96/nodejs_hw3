const bcrypt = require("bcrypt");

const User = require("../models/user");
const Truck = require("../models/truck");
const Load = require("../models/load");

module.exports.hashPassword = async (password) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    return hash;
  } catch (error) {
    console.log(error);
  }
};

module.exports.checkHashPassword = async (passwordEnteredByUser, hash) => {
  try {
    const isSame = await bcrypt.compare(passwordEnteredByUser, hash);
    return isSame;
  } catch (error) {
    console.log(error);
  }
};

module.exports.findUserById = async (currentUserId) => {
  const user = await User.findById(currentUserId);
  if (!user) {
    throw { status: 400, message: "No user with such id found" };
  }
  return user;
};

module.exports.findTruckById = async (truckId, currentUserId) => {
  const truck = await Truck.findById(truckId);
  if (!truck) {
    throw { status: 400, message: "No truck with such id found" };
  }
  if (truck.created_by !== currentUserId) {
    throw { status: 403, message: "You don't have access to this truck" };
  }
  return truck;
};

module.exports.findLoadById = async (loadId, currentUserId) => {
  const load = await Load.findById(loadId);
  if (!load) {
    throw { status: 400, message: "No load with such id found" };
  }
  if (load.created_by !== currentUserId) {
    throw { status: 403, message: "You don't have access to this load" };
  }
  return load;
};

module.exports.checkIfLoadFitsInTrucks = (
  truckType,
  loadDimensions,
  payload
) => {
  if (
    truckType.height > loadDimensions.height &&
    truckType.width > loadDimensions.width &&
    truckType.length > loadDimensions.length &&
    truckType.payload > payload
  ) {
    return true;
  }
  return false;
};
