const mongoose = require("mongoose");

module.exports = mongoose.model("load", {
  name: {
    required: true,
    type: String,
  },
  pickup_address: {
    required: true,
    type: String,
  },
  delivery_address: {
    required: true,
    type: String,
  },
  created_date: {
    required: true,
    type: String,
  },
  created_by: {
    required: true,
    type: String,
  },
  status: {
    required: true,
    type: String,
  },
  dimensions: {
    required: true,
    type: {
      width: { required: true, type: Number },
      length: { required: true, type: Number },
      height: { required: true, type: Number },
    },
  },
  payload: { required: true, type: Number },
  logs: [
    {
      message: { required: true, type: String },
      time: { required: true, type: String },
    },
  ],
  state: {
    type: String,
  },
  assigned_to: {
    type: String,
  },
});
