const mongoose = require("mongoose");

module.exports = mongoose.model("user", {
  email: {
    required: true,
    type: String,
    unique: true,
  },
  password: {
    required: true,
    type: String,
  },
  created_date: {
    required: true,
    type: String,
  },
  role: {
    required: true,
    type: String,
  },
});
