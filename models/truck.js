const mongoose = require("mongoose");

module.exports = mongoose.model("truck", {
  created_by: {
    required: true,
    type: String,
  },
  status: {
    required: true,
    type: String,
  },
  type: {
    required: true,
    type: String,
  },
  created_date: {
    required: true,
    type: String,
  },
  assigned_to: {
    type: String,
  },
});
