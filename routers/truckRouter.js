const express = require("express");

const {
  addTruck,
  getCreatedTrucks,
  assignTruckToUser,
  updateTruckById,
  deleteTruckById,
  getTruckById,
} = require("../controllers/truckController");
const authMiddleware = require("../middlewares/authMiddleware");
const schemaValidatorMiddleware = require("../middlewares/schemaValidatorMiddleware");
const {
  driverValidatorMiddleware,
} = require("../middlewares/userRoleValidatorMiddleware");
const activeLoadCheckMiddleware = require("../middlewares/activeLoadCheckMiddleware");

const router = express.Router();

router.post(
  "/trucks",
  [authMiddleware, driverValidatorMiddleware, schemaValidatorMiddleware],
  addTruck
);
router.get(
  "/trucks/:id",
  [authMiddleware, driverValidatorMiddleware],
  getTruckById
);
router.get(
  "/trucks",
  [authMiddleware, driverValidatorMiddleware],
  getCreatedTrucks
);
router.post(
  "/trucks/:id/assign",
  [authMiddleware, driverValidatorMiddleware, activeLoadCheckMiddleware],
  assignTruckToUser
);
router.put(
  "/trucks/:id",
  [
    authMiddleware,
    driverValidatorMiddleware,
    schemaValidatorMiddleware,
    activeLoadCheckMiddleware,
  ],
  updateTruckById
);
router.delete(
  "/trucks/:id",
  [authMiddleware, driverValidatorMiddleware, activeLoadCheckMiddleware],
  deleteTruckById
);

module.exports = router;
