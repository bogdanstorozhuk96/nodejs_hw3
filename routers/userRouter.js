const express = require("express");

const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require("../controllers/userController");
const authMiddleware = require("../middlewares/authMiddleware");
const schemaValidatorMiddleware = require("../middlewares/schemaValidatorMiddleware");
const activeLoadCheckMiddleware = require("../middlewares/activeLoadCheckMiddleware");

const router = express.Router();

router.get("/users/me", authMiddleware, getUser);
router.delete(
  "/users/me",
  [authMiddleware, activeLoadCheckMiddleware],
  deleteUser
);
router.patch(
  "/users/me/password",
  [authMiddleware, schemaValidatorMiddleware, activeLoadCheckMiddleware],
  changeUserPassword
);

module.exports = router;
