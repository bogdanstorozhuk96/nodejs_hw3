const express = require("express");

const {
  addLoad,
  getCreatedLoads,
  updateLoad,
  deleteLoadWithStatusNewById,
  postLoad,
  getShippingInfo,
  getLoadById,
  getActiveDriverLoad,
  iterateToNextLoadState,
} = require("../controllers/loadController");
const authMiddleware = require("../middlewares/authMiddleware");
const schemaValidatorMiddleware = require("../middlewares/schemaValidatorMiddleware");
const querySchemaValidatorMiddleware = require("../middlewares/querySchemaValidatorMiddleware");
const {
  shipperValidatorMiddleware,
  driverValidatorMiddleware,
} = require("../middlewares/userRoleValidatorMiddleware");

const router = express.Router();

router.post(
  "/loads",
  [authMiddleware, shipperValidatorMiddleware, schemaValidatorMiddleware],
  addLoad
);
router.get(
  "/loads",
  [authMiddleware, querySchemaValidatorMiddleware],
  getCreatedLoads
);
router.get(
  "/loads/active",
  [authMiddleware, driverValidatorMiddleware],
  getActiveDriverLoad
);
router.patch(
  "/loads/active/state",
  [authMiddleware, driverValidatorMiddleware],
  iterateToNextLoadState
);
router.put(
  "/loads/:id",
  [authMiddleware, shipperValidatorMiddleware, schemaValidatorMiddleware],
  updateLoad
);
router.delete(
  "/loads/:id",
  [authMiddleware, shipperValidatorMiddleware],
  deleteLoadWithStatusNewById
);
router.post(
  "/loads/:id/post",
  [authMiddleware, shipperValidatorMiddleware],
  postLoad
);
router.get(
  "/loads/:id",
  [authMiddleware, shipperValidatorMiddleware],
  getLoadById
);
router.get(
  "/loads/:id/shipping_info",
  [authMiddleware, shipperValidatorMiddleware],
  getShippingInfo
);

module.exports = router;
