const express = require("express");
const router = express.Router();

const {
  register,
  login,
  forgotPassword,
} = require("../controllers/authController");
const schemaValidatorMiddleware = require("../middlewares/schemaValidatorMiddleware");

router.post("/auth/register", schemaValidatorMiddleware, register);
router.post("/auth/login", schemaValidatorMiddleware, login);
router.post("/auth/forgot_password", schemaValidatorMiddleware, forgotPassword);

module.exports = router;
